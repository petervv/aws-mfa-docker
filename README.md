# build
docker build -t aws-mfa:latest -t aws-mfa .

# ~/bin/aws-mfa
```bash
#!/bin/bash

docker run \
 --rm \
 -it \
 -e "FILE_OWNER_ID=$(id -u)" \
 -e "GROUP_OWNER_ID=$(id -g)" \
 -v "$HOME/.aws:/home/user/.aws" \
 registry.gitlab.com/petervv/aws-mfa-docker \
 aws-mfa $*
```