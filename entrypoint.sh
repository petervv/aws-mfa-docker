#!/usr/bin/env bash

USER=user

# Check if group with id $GROUP_OWNER_ID exists
if [ -z "$(getent group $GROUP_OWNER_ID)" ]; then
    groupadd -g $GROUP_OWNER_ID $USER
fi

# Check if user with id $FILE_OWNER_ID exists
if [ -z "$(getent passwd $FILE_OWNER_ID)" ]; then
    # work around volume mount
    useradd -m -u $FILE_OWNER_ID -g $GROUP_OWNER_ID -s /bin/bash -d /home/${USER}_ $USER
    mv /home/${USER}_/* /home/${USER} 2>/dev/null
    mv /home/${USER}_/.* /home/${USER} 2>/dev/null
    rmdir /home/${USER}_
    usermod -d /home/${USER} $USER
fi

exec /usr/local/bin/gosu $USER "$@"
